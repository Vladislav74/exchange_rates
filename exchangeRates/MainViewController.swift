//
//  ViewController.swift
//  exchange_rates_new
//
//  Created by Владислав on 19.03.16.
//  Copyright © 2016 Vladislav Zhukov. All rights reserved.
//
import UIKit

let backgroundColor = UIColor.init(red: 61.0/255, green: 135.0/255, blue: 221.0/255, alpha: 1)

class MainViewController: UIViewController, UIPageViewControllerDataSource {

    var pageViewController: UIPageViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.pageViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PageViewController") as! UIPageViewController
        self.pageViewController.dataSource = self

        let startVC = self.viewControllerAtIndex(0) as ContentViewController
        let viewControllers = NSArray(object: startVC)
        
        self.pageViewController.setViewControllers(viewControllers as! [UIViewController] as [UIViewController], direction: .Forward, animated: true, completion: nil)
        self.pageViewController.view.frame = CGRectMake(0, 30, self.view.frame.width, self.view.frame.size.height)
        
        self.addChildViewController(self.pageViewController)
        self.view.addSubview(self.pageViewController.view)
        self.pageViewController.didMoveToParentViewController(self)
    }
    
    func viewControllerAtIndex(index: Int) -> ContentViewController {
        
        if ((ERengine.sharedInstance.userSetting.listOfValutaAndResources.count == 0) || (index >= ERengine.sharedInstance.userSetting.listOfValutaAndResources.count)) {
            
            let newIndex = ERengine.sharedInstance.userSetting.listOfValutaAndResources.count - 1
            let vc: ContentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ContentViewController") as! ContentViewController
            vc.valutaName = ERengine.sharedInstance.userSetting.listOfValutaAndResources[newIndex]
            vc.pageIndex = newIndex

            return vc
        }
        
        let vc: ContentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ContentViewController") as! ContentViewController
        
        vc.valutaName = ERengine.sharedInstance.userSetting.listOfValutaAndResources[index]
        vc.pageIndex = index
        
        return vc
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController?
    {
        let vc = viewController as! ContentViewController
        var index = vc.pageIndex as Int
        
        if (index == 0 || index == NSNotFound) {
        
            return nil
        }
        index -= 1
        
        return self.viewControllerAtIndex(index)
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController?{
        
        let vc = viewController as! ContentViewController
        var index = vc.pageIndex as Int
        
        if (index == NSNotFound) {
            
            return nil
        }
        
        index += 1
        
        if (index == ERengine.sharedInstance.userSetting.listOfValutaAndResources.count) {
            
            return nil
        }
        
        return self.viewControllerAtIndex(index)
    }
    
    /*
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return UserSettings.sharedInstance.listOfValutaAndResources.count
    }
    */
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
}







