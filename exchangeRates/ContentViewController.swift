//
//  ContentViewController.swift
//  exchange_rates_new
//
//  Created by Владислав on 21.03.16.
//  Copyright © 2016 Vladislav Zhukov. All rights reserved.
//
import UIKit
import JBChart

class ContentViewController: UIViewController, JBLineChartViewDelegate, JBLineChartViewDataSource {
    
    @IBOutlet weak var valutaLabel: UILabel!
    @IBOutlet weak var valutaCourse: UILabel!
    @IBOutlet weak var image: UIImageView! //FIXME: rename
    @IBOutlet weak var lineChart: JBLineChartView!
    @IBOutlet weak var labelWithUpdateTime: UILabel!
    @IBOutlet weak var labelWithCourseOfValutaForDate: UILabel!
    @IBOutlet weak var labelForChartHeader: UILabel!
    @IBOutlet weak var labelWithPercentageСhange: UILabel! //FIXME: rename
    @IBOutlet weak var labelWithNominal: UILabel!
    @IBOutlet weak var labelWithValutaNameRus: UILabel!
    
    var pageIndex : Int!
    var valutaName : String!
    var valutaData : CoursesData!
    
    @IBAction func CalcAction(sender: AnyObject) {
        
        let toShow = self.storyboard?.instantiateViewControllerWithIdentifier("CalcView") as! CalcViewController
        toShow.nominal = valutaData!.todayNominal ?? 0.0
        toShow.value = valutaData!.todayValue ?? 0.0
        presentViewController(toShow, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.labelForChartHeader.text = "Динамика курса \(valutaName) к рублю"
        self.labelWithValutaNameRus.text = ERengine.sharedInstance.valutaRusName[valutaName]
        self.image.image = UIImage(named: "arrowDown")
        self.valutaLabel.text = valutaName
        self.valutaCourse.text = "--"
        
        view.backgroundColor = backgroundColor
        // line chart setup
        lineChart.backgroundColor = backgroundColor
        lineChart.delegate = self
        lineChart.dataSource = self
        lineChart.minimumValue = 55
        lineChart.maximumValue = 100
        
        hideChart()
        
        ERengine.sharedInstance.getDataForValuta(valutaName: valutaName) { [weak weakSelf = self] (data) in
            
            weakSelf?.valutaData = data
            weakSelf?.updateData()
        }
    }
    
    func updateData() {

        self.valutaCourse.text = valutaName
        let valueAsString = String(valutaData!.todayValue) ?? "0"
        self.valutaCourse.text = valueAsString
        let nominalAsString = String(valutaData!.todayNominal) ?? "0"
        self.labelWithNominal.text = nominalAsString + "="
        let dataAsString = valutaData!.dateValue ?? "00/00/0000"
        self.labelWithUpdateTime.text = "Обновлено: \(dataAsString)"
        self.labelWithCourseOfValutaForDate.text = "Курс на " + dataAsString + " " + valueAsString + " руб"
        let unwrappedDifferenceValue = valutaData!.difference ?? 0.0
        let differenceAsString = String(unwrappedDifferenceValue) ?? "0"
        self.labelWithPercentageСhange.text = differenceAsString
        
        if valutaData!.bValueIsIncreases {
           self.image.image = UIImage(named: "arrowUp")
        } else {
           self.image.image = UIImage(named: "arrowDown")
        }

        self.lineChart.reloadDataAnimated(true)
        showChart()
        _ = NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: #selector(ContentViewController.showChart), userInfo: nil, repeats: false)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        ERengine.sharedInstance.getDataForValuta(valutaName: valutaName) { [weak weakSelf = self] (data) in
            
            weakSelf?.valutaData = data
            weakSelf?.updateData()
        }

    }
    
    func hideChart() {
        lineChart.setState(.Collapsed, animated: true)
    }
    
    func showChart() {
        lineChart.setState(.Expanded, animated: true)
    }
    
    // MARK: JBlineChartView
    func numberOfLinesInLineChartView(lineChartView: JBLineChartView!) -> UInt {
        return 1
    }
    
    func lineChartView(lineChartView: JBLineChartView!, numberOfVerticalValuesAtLineIndex lineIndex: UInt) -> UInt {
        return UInt(valutaData!.values.count)
    }
    
    func lineChartView(lineChartView: JBLineChartView!, verticalValueForHorizontalIndex horizontalIndex: UInt, atLineIndex lineIndex: UInt) -> CGFloat {
        
       return CGFloat(valutaData!.values[Int(horizontalIndex)])
    }
    
    func lineChartView(lineChartView: JBLineChartView!, colorForLineAtLineIndex lineIndex: UInt) -> UIColor! {
        return UIColor.whiteColor()
    }
    
    func lineChartView(lineChartView: JBLineChartView!, widthForLineAtLineIndex lineIndex: UInt) -> CGFloat {
        return 1.3
    }
    
    func lineChartView(lineChartView: JBLineChartView!, showsDotsForLineAtLineIndex lineIndex: UInt) -> Bool {
        return false
    }
    
    func lineChartView(lineChartView: JBLineChartView!, colorForDotAtHorizontalIndex horizontalIndex: UInt, atLineIndex lineIndex: UInt) -> UIColor! {
        return UIColor.whiteColor()
    }
    
    func lineChartView(lineChartView: JBLineChartView!, smoothLineAtLineIndex lineIndex: UInt) -> Bool {
        
        return false
    }
    
    func lineChartView(lineChartView: JBLineChartView!, didSelectLineAtIndex lineIndex: UInt, horizontalIndex: UInt) {

        let valueAsString = String(valutaData!.values[Int(horizontalIndex)]) ?? "0"
        let dataAsString = valutaData!.dates[Int(horizontalIndex)] ?? "00/00/0000"
        self.labelWithCourseOfValutaForDate.text = "Курс на \(dataAsString) \(valueAsString) руб"
    }
    
    func didDeselectLineInLineChartView(lineChartView: JBLineChartView!) {
    }
    
    func lineChartView(lineChartView: JBLineChartView!, fillColorForLineAtLineIndex lineIndex: UInt) -> UIColor! {
        return backgroundColor
    }
}







