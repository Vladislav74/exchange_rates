//
//  SettingsViewController.swift
//  exchangeRates
//
//  Created by Владислав on 13.04.16.
//  Copyright © 2016 MD software. All rights reserved.
//
import UIKit

class SettingsViewController : UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBAction func ButtonHide(sender: UIButton) {
        
        ERengine.sharedInstance.userSetting.saveUserConfig()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = backgroundColor
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundColor = backgroundColor
        return ERengine.sharedInstance.userSetting.possibleListOfValutaAndResources.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellId = "settingCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellId, forIndexPath: indexPath) as! CellViewController
        
        let valutaName = ERengine.sharedInstance.userSetting.keyForIndexForPossibleListOfValutaAndResources[indexPath.row]
        
        cell.backgroundColor = backgroundColor
        let imageName = ERengine.sharedInstance.valutaFlagImageName[valutaName]!
        cell.flagImageOfValuta.image = UIImage(named: imageName)
        cell.Lable.text = valutaName
        cell.Switch.on = ERengine.sharedInstance.userSetting.possibleListOfValutaAndResources[valutaName]!
        
        return cell
    }
    
}