//
//  UserSettings.swift
//  exchange_rates_new
//
//  Created by Владислав on 24.03.16.
//  Copyright © 2016 Vladislav Zhukov. All rights reserved.
//
import Foundation


class UserSettings {

    /*
class var sharedInstance :UserSettings {
    
    
        struct Singleton {
           static let instance = UserSettings()
       }
       return Singleton.instance
    }
    */
    
    let startSettings = ["listOfValutaAndResources" : ["USD", "EUR"]]

    let keyForIndexForPossibleListOfValutaAndResources = ["USD", "EUR", "GBP", "UAH", "CHF", "BYR", "KZT"]
    
    var possibleListOfValutaAndResources = [
        "USD" : false,
        "EUR" : false,
        "GBP" : false,
        "UAH" : false,
        "CHF" : false,
        "BYR" : false,
        "KZT" : false
    ]
    
    var listOfValutaAndResources = [String]()
    
    init() {
        
       let defaults = NSUserDefaults.standardUserDefaults()
       listOfValutaAndResources = defaults.arrayForKey("listOfValutaAndResources") as! [String]
        
        for valuta in listOfValutaAndResources {
            possibleListOfValutaAndResources.updateValue(true, forKey: valuta)
        }
    }
    
    func saveUserConfig() {
        var newListOfValutaAndResources = [String]()
        for (valutaCode, valutaSetting) in possibleListOfValutaAndResources {
            if valutaSetting {
                newListOfValutaAndResources.append(valutaCode)
            }
        }
        listOfValutaAndResources = newListOfValutaAndResources
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setValue(listOfValutaAndResources, forKey: "listOfValutaAndResources")
        ERengine.sharedInstance.settingWasUpdated()
    }
    
    /*
    func downloadUserConfig() {
        let defaults = NSUserDefaults.standardUserDefaults()
        listOfValutaAndResources = defaults.arrayForKey("listOfValutaAndResources") as! [String]
    }
    */
    
}
