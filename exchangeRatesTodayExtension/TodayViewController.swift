//
//  TodayViewController.swift
//  exchangeRatesTodayExtension
//
//  Created by Владислав on 06.06.16.
//  Copyright © 2016 MD software. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController, NCWidgetProviding, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tabelHeight: NSLayoutConstraint!
    
    let backgroundColor = UIColor.init(red: 61.0/255, green: 135.0/255, blue: 221.0/255, alpha: 1)
    
    var userSettingForTE = SettingsForTodayExtension()
    
    /*
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        userSetting = SettingsForTodayExtension()
    }*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = backgroundColor
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundColor = backgroundColor
        let numberOfRow = userSettingForTE.listOfSelectedValutes.count
        tabelHeight.constant = CGFloat(numberOfRow) * 38
        return numberOfRow
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellId = "todayExtensionCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellId, forIndexPath: indexPath) as! TodayExtensionCell
        
        let valutaName = userSettingForTE.listOfSelectedValutes[indexPath.row]
        
        cell.valutaPriceLabel.text = "--"
        cell.valutaTrendImage.image = UIImage(named: "arrowUp") //FIXME: image not shown
        
        ERengine.sharedInstance.getDataForValuta(valutaName: valutaName) { (data) in

            let valueAsString = String(data.todayValue) ?? "0"
            cell.valutaPriceLabel.text = valueAsString
            
            if data.bValueIsIncreases {
                cell.valutaTrendImage.image = UIImage(named: "arrowUp")
            } else {
                cell.valutaTrendImage.image  = UIImage(named: "arrowDown")
            }
        }
        
        cell.backgroundColor = backgroundColor
        let imageName = ERengine.sharedInstance.valutaFlagImageName[valutaName]!
        
        cell.valutaFlagImage.image = UIImage(named: imageName)
        cell.valutaNameLabel.text = valutaName
        
        return cell
    }
    
    func widgetPerformUpdateWithCompletionHandler(completionHandler: ((NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.

        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData

        completionHandler(NCUpdateResult.NewData)
    }
    
}
