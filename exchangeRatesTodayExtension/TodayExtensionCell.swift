//
//  TodayExtensionCell.swift
//  exchangeRates
//
//  Created by Владислав on 06.06.16.
//  Copyright © 2016 MD software. All rights reserved.
//

import UIKit

class TodayExtensionCell: UITableViewCell {
    
    @IBOutlet weak var valutaFlagImage: UIImageView!
    @IBOutlet weak var valutaNameLabel: UILabel!
    @IBOutlet weak var valutaTrendImage: UIImageView!
    @IBOutlet weak var valutaPriceLabel: UILabel!

}